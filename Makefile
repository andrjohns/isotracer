### * Setup

TOP_DIR=$(shell git rev-parse --show-toplevel)

### ** Colors

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** TODO

## TODO : Search for TODO items in .R and .Rmd.original files
.PHONY: TODO
TODO:
	find . -name "*.R" -o -name "*.Rmd*" | xargs grep -i TODO --color=always

# ### ** analysis

# ## analysis : run analysis scripts
# .PHONY: analysis
# analysis: install
# 	@printf "\n"
# 	@printf "$(GREEN)*** Running analysis scripts ***$(NC)\n"
# 	@printf "\n"

### ** data

## data : build the rda data files included in the package
.PHONY: data
data:
	@printf "\n"
	@printf "$(GREEN)*** Building package data files ***$(NC)\n"
	@printf "\n"
	@mkdir -p data/
	# Models and runs
	@cd prep-data/; Rscript prep-run-example.R
	@cd prep-data/; Rscript prep-large-model.R
	# Case studies datasets
	@cd prep-data/; Rscript prep-dataset-mcroy1970.R
	@cd prep-data/; Rscript prep-dataset-li2017.R
	@cd prep-data/; Rscript prep-dataset-collins2016.R
	# Clean-up
	@cd prep-data/; rm -f Rplots.pdf

### ** README

## README : generate README.md from README.Rmd
.PHONY: README
README:
	@printf "\n"
	@printf "$(GREEN)*** Updating 'README.md' from 'README.Rmd' ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'rmarkdown::render("README.Rmd", output_format = rmarkdown::github_document(html_preview = FALSE), quiet = TRUE)'

### ** document

## document : generate package documentation using Roxygen (also compiles src files)
.PHONY: document
document: quick data README
	@printf "\n"
	@printf "$(GREEN)*** Generating package documentation with Roxygen ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'devtools::document(quiet = TRUE)'

### ** quick

## quick : "quick" install the package (no "data", no "document", does not recompile Cpp sources)
.PHONY: quick
quick:
	@printf "\n"
	@printf "$(GREEN)*** Installing the package ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'rstantools::rstan_config()'
	@Rscript -e 'devtools::install(upgrade = FALSE, quick = TRUE, quiet = TRUE)'

### ** install

## install : install the package (also runs "data" and "document")
.PHONY: install
install:  # Should also have "data" as the first dependency
	make document
	make clean-compiled
	make quick
	@printf "\n"
	@printf "$(GREEN)*** Package installed ***$(NC)\n"
	@printf "\n"

# all : alias for install
.PHONY: all
all: install

### ** recompile

## recompile : delete compiled code and install the package
.PHONY: recompile
recompile: clean-compiled install

### ** test

## test : run package tests
.PHONY: test
test:
	@printf "\n"
	@printf "$(GREEN)*** Running package tests ***$(NC)\n"
	@printf "\n"
	@Rscript -e "library(devtools); test()"
	@cd tests/testthat; rm -f Rplots.pdf

### ** test-cs

## test-cs : test case studies
.PHONY: test-cs
test-cs:
	@printf "\n"
	@printf "$(GREEN)*** Testing case studies ***$(NC)\n"
	@printf "\n"
	@cd tests-case-studies; Rscript -e "devtools::load_all(); library(testthat); test_file('test-case-studies_collins-2016.R')"
	@cd tests-case-studies; Rscript -e "devtools::load_all(); library(testthat); test_file('test-case-studies_li-2017.R')"
	@cd tests-case-studies; Rscript -e "devtools::load_all(); library(testthat); test_file('test-case-studies_mcroy-1970.R')"

### ** coverage

## coverage : determine test coverage
.PHONY: coverage
coverage:
	@printf "\n"
	@printf "$(GREEN)*** Determining test coverage ***$(NC)\n"
	@printf "\n"
	@mkdir -p docs/coverage/
	@Rscript -e "library(covr); cov = package_coverage(); report(cov, \"$(TOP_DIR)/docs/coverage/coverage.html\"); print(paste(\"Coverage_percent: --\", round(percent_coverage(cov), 2), \"--\"))"

### ** precompile-doc

## precompile-doc : precompile Rmd vignettes from Rmd.original files
.PHONY: precompile-doc
precompile-doc:
	@printf "\n"
	@printf "$(GREEN)*** Precompiling vignettes from *.Rmd.original files ***$(NC)\n"
	@printf "\n"
	cd vignettes; Rscript -e 'knitr::knit("tutorial-010-quick-start.Rmd.original", output = "tutorial-010-quick-start.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-020-replication.Rmd.original", output = "tutorial-020-replication.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-030-steady-state-comps.Rmd.original", output = "tutorial-030-steady-state-comps.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-040-pulse-drip-events.Rmd.original", output = "tutorial-040-pulse-drip-events.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-050-fixed-effects.Rmd.original", output = "tutorial-050-fixed-effects.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-060-units-priors.Rmd.original", output = "tutorial-060-units-priors.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-070-prior-predictive-checks.Rmd.original", output = "tutorial-070-prior-predictive-checks.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-080-mcmc-output-format.Rmd.original", output = "tutorial-080-mcmc-output-format.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-090-post-run-analyses.Rmd.original", output = "tutorial-090-post-run-analyses.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-100-posterior-predictive-checks.Rmd.original", output = "tutorial-100-posterior-predictive-checks.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-110-derived-parameters.Rmd.original", output = "tutorial-110-derived-parameters.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-120-howto-simulations.Rmd.original", output = "tutorial-120-howto-simulations.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("tutorial-130-parameter-identifiability.Rmd.original", output = "tutorial-130-parameter-identifiability.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("case-study-mcroy-1970.Rmd.original", output = "case-study-mcroy-1970.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("case-study-li-2017.Rmd.original", output = "case-study-li-2017.Rmd")'
	cd vignettes; Rscript -e 'knitr::knit("case-study-collins-2016.Rmd.original", output = "case-study-collins-2016.Rmd")'

### ** pkgdown

## pkgdown : build the package website using pkgdown (includes manuscript draft)
# TODO: https://community.rstudio.com/t/how-to-generate-a-reference-for-all-non-exported-functions-with-pkgdown/28894
.PHONY: pkgdown
pkgdown:
	@printf "\n"
	@printf "$(GREEN)*** Building package website with pkgdown ***$(NC)\n"
	@printf "\n"
	@rm -fr docs/*
	@cp pkgdown/_pkgdown.master.yml pkgdown/_pkgdown.yml
	@Rscript .updatePkgdown.R
	@Rscript -e 'pkgdown::build_site()'
	@cp CRAN-version_badge.svg docs/

### ** buildVignettes

## buildVignettes : build the vignettes and put them into inst/doc
.PHONY: buildVignettes
buildVignettes: 
	@printf "\n"
	@printf "$(GREEN)*** Building vignettes with 'devtools::build_vignettes()' ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'devtools::build_vignettes()'

### ** check

## check : run R CMD CHECK through devtools check() function
.PHONY: check
check:
	@printf "\n"
	@printf "$(GREEN)*** Running 'devtools::check()' ***$(NC)\n"
	@printf "\n"
	@Rscript .run_check_and_get_badge.R

### ** uninstall

## uninstall : uninstall the package
.PHONY: uninstall
uninstall:
	@printf "\n"
	@printf "$(GREEN)*** Uninstalling the package ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'tryCatch(remove.packages("isotracer"), error = function(e) {})'

### ** uncache

## uncache : delete cache files used for vignette rendering
.PHONY: uncache
uncache:
	@printf "\n"
	@printf "$(GREEN)*** Removing cache files ***$(NC)\n"
	@printf "\n"
	@rm -fr vignettes/*.cache

### ** clean

## clean : delete all files and folders generated during building process and analysis
.PHONY: clean
clean: clean-data clean-man clean-docs clean-vignettes clean-compiled clean-prep-data-cache
	@printf "\n"
	@printf "$(GREEN)*** Cleaned generated files and folders ***$(NC)\n"
	@printf "\n"
	@rm -f R-CMD-check_output.txt
	@rm -f R-CMD-check_badge.svg
	@rm -f rhub-report_*

### ** clean-not-cache

## clean-not-cache : same as clean but keep vignettes and prep-data cache files
.PHONY: clean-not-cache
clean-not-cache: clean-data clean-man clean-docs clean-vignettes-not-cache clean-compiled
	@printf "\n"
	@printf "$(GREEN)*** Cleaned generated files and folders ***$(NC)\n"
	@printf "\n"
	@rm -f R-CMD-check_output.txt
	@rm -f R-CMD-check_badge.svg

### ** clean-data

## clean-data : delete all package data files built from prep_data/
.PHONY: clean-data
clean-data:
	@rm -f data/*
	@rm -f inst/extdata/*
	@rm -f R/sysdata.rda
## clean-prep-data-cache : delete cache files in prep_data/
.PHONY: clean-prep-data-cache
clean-prep-data-cache:
	@rm -f prep-data/z-cache-prep-run-example.rds

### ** clean-man

## clean-man : delete documentation generated by Roxygen
.PHONY: clean-man
clean-man:
	@rm -f man/*.Rd

### ** clean-docs

## clean-docs : delete documentation generated by pkgdown
.PHONY: clean-docs
clean-docs:
	@rm -fr docs/*
	@rm -f _pkgdown.yml
	@rm -f vignettes/class-auto-doc.Rmd
	@rm -fr vignettes/test-movie vignettes/*.mp4
	@rm -f vignettes/*.RData
	@rm -f vignettes/profiling.html
	@rm -f description-R6-classes.org
	@rm -f sandbox/profiling.html
	@rm -fr sandbox/profiling_files

### ** clean-vignettes

## clean-vignettes : clean the vignettes
.PHONY: clean-vignettes
clean-vignettes: 
	@rm -fr inst/docs
	@cd vignettes; rm -f *.Rmd *.html figures/* *.rds *.RData

### ** clean-vignettes-not-cache

## clean-vignettes-not-cache : clean the vignettes but not the vignette cache
.PHONY: clean-vignettes-not-cache
clean-vignettes-not-cache: 
	@rm -fr inst/docs
	@cd vignettes; rm -f *.Rmd *.html figures/*

### ** clean-compiled

## clean-compiled : delete compiled code
.PHONY: clean-compiled
clean-compiled:
	@rm -fr src/

### ** prerelease-check

## prerelease-check : uninstall, clean, install, test, precompile-doc, pkgdown, check
.PHONY: prerelease-check
prerelease-check: uninstall clean install test precompile-doc pkgdown check

### * Notes

# To replace the graphic device used in vignettes, use something like (from vignettes/):
# find . -name "*.Rmd" | xargs sed -i 's/fig.align = \"center\", dev = \"jpeg\"/fig.align = \"center\"/g'
