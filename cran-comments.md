## Reason for submitting an updated version of the package (1.1.3 -> 1.1.4)

- A contribution from one of the developers of rstantools (Andrew Johnson)
  modified the package installation/configuration so that it is now delegated
  to rstantools. This should ensure that the package is compatible with future
  versions of rstan/StanHeaders.

- Some bugs were fixed in the code, and a vignette for which the code was not
  running properly was fixed.


## Summary of update

- Modify `DESCRIPTION` and add files `configure` and `configure.win` to
  delegate package installation/configuration to rstantools.

- Fix bugs in `sample_from()`, `quick_sankey()`, and `traceplot()`.

- Fix the vignette "How to simulate experiments" so that the code is now
  running correctly.

- Package version was bumped to 1.1.4.


## Test environments (on 2023-03-20)

- Local: Debian 5.10.162-1, R 4.0.4
- R-hub: macOS 10.13.6 High Sierra, R-release, brew
- R-hub: Windows Server 2022, R-release, 32/64 bit


## Summary of R CMD check results

There were no ERRORs or WARNINGs.

There were 3 NOTES:

**  checking installed package size ... NOTE
  installed size is  7.6Mb
  sub-directories of 1Mb or more:
    data   1.8Mb
    doc    2.3Mb
    libs   2.4Mb 

The size of the locally built tarball is 4018281 bytes, respecting the 5 MB
limit for CRAN packages.


**  checking dependencies in R code ... NOTE
Namespace in Imports field not imported from: ‘rstantools’
  All declared Imports should be used.

'rstantools' was added to the Imports field so that the package
installation/configuration is delegated to rstantools for compatibility with
future releases of rstan/StanHeaders. While rstantools is not used by our
package code in the './R/' folder, it is used in the files './configure' and
'./configure.win'.


**  checking for GNU extensions in Makefiles ... NOTE
GNU make is a SystemRequirements. 

The GNU make requirement is introduced by the dependency on the rstan package.


## Downstream dependencies

There are currently no downstream dependencies for this package.
